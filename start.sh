#!/bin/bash

# Starting Traefik and Portainer
cd core/
docker-compose up -d

# Starting www
cd ../www/
docker-compose up -d

# Go to root
cd ../
