#!/bin/bash

# stop services
. stop.sh

# update git
git pull

# create webproxy network
docker network create webproxy

# start services
. start.sh
