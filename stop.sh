#!/bin/bash

# Shut down Traefik and Portainer
cd core/
docker-compose down

# Shut down www
cd ../www/
docker-compose down

# Go to root
cd ../
